#!/bin/sh
set -eu

# Command line check.
if [ $# -lt 2 ] || [ $# -gt 3 ]|| [ "$1" != "--upstream-version" ]; then
    echo "$0: This script must be called via uscan." >&2
    exit 1
fi
version="$2"
tarball="../socklog_$version.orig.tar.gz"

# Create a temporary directory and delete it on exit.
temp="$(mktemp -d)"
trap '! [ -d "$temp" ] || rm -rf -- "$temp"' EXIT

# Unpack the original tarball, stripping the first directory component.
tar -C "$temp" -xf "$tarball" --strip-components=1

# Rename the toplevel directory.
if [ "$(basename "$temp"/socklog-*)" != socklog-"$version" ]; then
    mv "$temp"/socklog-* "$temp"/socklog-"$version"
fi

# Repack it, overwriting the original tarball.
rm -f "$tarball"
tar -C "$temp" -czf "$tarball" --owner root --group root "socklog-$version"
